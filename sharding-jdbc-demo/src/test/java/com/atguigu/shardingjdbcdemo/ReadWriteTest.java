package com.atguigu.shardingjdbcdemo;

import com.atguigu.shardingjdbcdemo.entity.Order;
import com.atguigu.shardingjdbcdemo.entity.User;
import com.atguigu.shardingjdbcdemo.mapper.OrderMapper;
import com.atguigu.shardingjdbcdemo.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * className: ReadWriteTest
 * description:
 * date: 2024/6/7-23:38
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingjdbcdemo
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@SpringBootTest
public class ReadWriteTest {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrderMapper orderMapper;

    /**
     * 写入数据的测试
     */
    @Test
    public void testInsert() {
        User user = new User();
        user.setUname("张三丰");
        userMapper.insert(user);
    }

    @Transactional
    @Test
    public void testTrans() {
        User user = new User();
        user.setUname("铁锤");
        userMapper.insert(user);

        List<User> users = userMapper.selectList(null);
    }

    /**
     * 负载均衡测试
     */
    @Test
    public void testSelectAll() {
        List<User> users1 = userMapper.selectList(null);
        List<User> users2 = userMapper.selectList(null);
        List<User> users3 = userMapper.selectList(null);
        List<User> users4 = userMapper.selectList(null);
        // users.forEach(System.out::println);
    }

    @Test
    public void testInsertOrderAndUser() {
        User user = new User();
        user.setUname("强哥");
        userMapper.insert(user);

        Order order = new Order();
        order.setUserId(user.getId());
        order.setAmount(new BigDecimal(100));
        order.setOrderNo("ATGUIGU001");
        orderMapper.insert(order);
    }

    @Test
    public void testSelectFromOrderAndUser() {
        User user = userMapper.selectById(1L);
        Order order = orderMapper.selectById(1L);
    }
}
