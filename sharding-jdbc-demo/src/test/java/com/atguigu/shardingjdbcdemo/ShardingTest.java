package com.atguigu.shardingjdbcdemo;

import com.atguigu.shardingjdbcdemo.entity.*;
import com.atguigu.shardingjdbcdemo.mapper.DictMapper;
import com.atguigu.shardingjdbcdemo.mapper.OrderItemMapper;
import com.atguigu.shardingjdbcdemo.mapper.OrderMapper;
import com.atguigu.shardingjdbcdemo.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * className: ShardingTest
 * description:
 * date: 2024/6/11-20:07
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingjdbcdemo
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@SpringBootTest
public class ShardingTest {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderItemMapper orderItemMapper;
    @Autowired
    private DictMapper dictMapper;

    /**
     * 写入数据的测试
     */
    @Test
    public void testInsert() {
        User user = new User();
        user.setUname("张三丰");
        userMapper.insert(user);
    }

    @Transactional
    @Test
    public void testTrans() {
        User user = new User();
        user.setUname("铁锤");
        userMapper.insert(user);

        List<User> users = userMapper.selectList(null);
    }

    /**
     * 负载均衡测试
     */
    @Test
    public void testSelectAll() {
        List<User> users1 = userMapper.selectList(null);
        List<User> users2 = userMapper.selectList(null);
        List<User> users3 = userMapper.selectList(null);
        List<User> users4 = userMapper.selectList(null);
        // users.forEach(System.out::println);
    }

    @Test
    public void testInsertOrderAndUser() {
        User user = new User();
        user.setUname("强哥");
        userMapper.insert(user);

        Order order = new Order();
        order.setUserId(user.getId());
        order.setAmount(new BigDecimal(100));
        order.setOrderNo("ATGUIGU001");
        orderMapper.insert(order);
    }

    @Test
    public void testSelectFromOrderAndUser() {
        User user = userMapper.selectById(1L);
        Order order = orderMapper.selectById(1L);
    }

    /**
     * 水平分片测试
     */
    @Test
    public void testInsertOrder() {
        Order order = new Order();
        order.setUserId(1L);
        order.setAmount(new BigDecimal(100));
        order.setOrderNo("ATGUIGU001");
        orderMapper.insert(order);
    }

    /**
     * 水平分片：分库插入数据测试
     */
    @Test
    public void testInsertOrderDatabaseStrategy() {
        for (long i = 4; i < 8; i++) {
            Order order = new Order();
            order.setUserId(i + 1);
            order.setAmount(new BigDecimal(100));
            order.setOrderNo("ATGUIGU001");
            orderMapper.insert(order);
        }
    }

    /**
     * 水平分片：分表插入数据测试
     */
    @Test
    public void testInsertOrderTableStrategy() {
        for (long i = 1; i < 5; i++) {
            Order order = new Order();
            order.setUserId(1L);
            order.setAmount(new BigDecimal(100));
            order.setOrderNo("ATGUIGU" + i);
            orderMapper.insert(order);
        }
        for (long i = 5; i < 9; i++) {
            Order order = new Order();
            order.setUserId(2L);
            order.setAmount(new BigDecimal(100));
            order.setOrderNo("ATGUIGU" + i);
            orderMapper.insert(order);
        }
    }

    @Test
    public void testHashMod() {
        System.out.println("ATGUIGU00100".hashCode() % 2);
        System.out.println("ATGUIGU00101".hashCode() % 2);
        System.out.println("ATGUIGU00102".hashCode() % 2);
        System.out.println("ATGUIGU00103".hashCode() % 2);
    }

    /**
     * 水平分片：查询所有记录
     */
    @Test
    public void testShardingSelectAll() {
        List<Order> orders = orderMapper.selectList(null);
        orders.forEach(System.out::println);
    }

    /**
     * 水平分片：根据user_id查询所有记录
     */
    @Test
    public void testShardingSelectByUserId() {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", 1L);
        List<Order> orders = orderMapper.selectList(wrapper);
        orders.forEach(System.out::println);
    }

    /**
     * 测试关联表插入
     */
    @Test
    public void testInsertOrderAndOrderItem() {

        for (long i = 1; i < 3; i++) {

            Order order = new Order();
            order.setOrderNo("ATGUIGU" + i);
            order.setUserId(1L);
            orderMapper.insert(order);

            for (long j = 1; j < 3; j++) {
                OrderItem orderItem = new OrderItem();
                orderItem.setOrderNo("ATGUIGU" + i);
                orderItem.setUserId(1L);
                orderItem.setPrice(new BigDecimal(10));
                orderItem.setCount(2);
                orderItemMapper.insert(orderItem);
            }
        }

        for (long i = 5; i < 7; i++) {

            Order order = new Order();
            order.setOrderNo("ATGUIGU" + i);
            order.setUserId(2L);
            orderMapper.insert(order);

            for (long j = 1; j < 3; j++) {
                OrderItem orderItem = new OrderItem();
                orderItem.setOrderNo("ATGUIGU" + i);
                orderItem.setUserId(2L);
                orderItem.setPrice(new BigDecimal(1));
                orderItem.setCount(3);
                orderItemMapper.insert(orderItem);
            }
        }

    }

    /**
     * 测试关联表查询
     */
    @Test
    public void testGetOrderAmount() {
        List<OrderVo> orderAmountList = orderMapper.getOrderAmount();
        orderAmountList.forEach(System.out::println);
    }

    /**
     * 广播表：每个服务器中的t_dict同时添加了新数据
     */
    @Test
    public void testBroadcast() {
        Dict dict = new Dict();
        dict.setDictType("type1");
        dictMapper.insert(dict);
    }

    /**
     * 查询操作，只从一个节点获取数据
     * 随机负载均衡规则
     */
    @Test
    public void testSelectBroadcast() {
        List<Dict> dicts = dictMapper.selectList(null);
        dicts.forEach(System.out::println);
    }

}
