package com.atguigu.shardingjdbcdemo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * className: ShardingJdbcDemoApplicationTests
 * description:
 * date: 2024/6/7-23:25
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingjdbcdemo
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@SpringBootTest
class ShardingJdbcDemoApplicationTests {

    @Test
    void contextLoads() {
    }

}
