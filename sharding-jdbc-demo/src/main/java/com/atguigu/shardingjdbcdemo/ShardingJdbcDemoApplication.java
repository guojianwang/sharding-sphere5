package com.atguigu.shardingjdbcdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * className: ShardingJdbcDemoApplication
 * description:
 * date: 2024/6/7-23:25
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingjdbcdemo
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@SpringBootApplication
public class ShardingJdbcDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShardingJdbcDemoApplication.class, args);
    }

}
