package com.atguigu.shardingjdbcdemo.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * className: OrderVo
 * description:
 * date: 2024/6/11-22:09
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingjdbcdemo.entity
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@Data
public class OrderVo {
    private String orderNo;
    private BigDecimal amount;
}
