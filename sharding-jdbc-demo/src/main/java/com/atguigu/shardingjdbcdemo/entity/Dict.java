package com.atguigu.shardingjdbcdemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * className: Dict
 * description:
 * date: 2024/6/11-22:26
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingjdbcdemo.entity
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@TableName("t_dict")
@Data
public class Dict {
    /**
     * 可以使用MyBatisPlus的雪花算法
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    private String dictType;
}