package com.atguigu.shardingjdbcdemo.controller;

import com.atguigu.shardingjdbcdemo.entity.User;
import com.atguigu.shardingjdbcdemo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * className: UserController
 * description:
 * date: 2024/6/11-18:02
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingjdbcdemo.controller
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @GetMapping("/selectAll")
    public void selectAll() {
        List<User> users = userMapper.selectList(null);
    }
}
