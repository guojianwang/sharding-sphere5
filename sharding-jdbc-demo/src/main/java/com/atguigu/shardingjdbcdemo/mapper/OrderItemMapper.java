package com.atguigu.shardingjdbcdemo.mapper;

import com.atguigu.shardingjdbcdemo.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * className: OrderItemMapper
 * description:
 * date: 2024/6/11-22:00
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingjdbcdemo.mapper
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItem> {

}
