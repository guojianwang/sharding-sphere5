package com.atguigu.shardingjdbcdemo.mapper;

import com.atguigu.shardingjdbcdemo.entity.Order;
import com.atguigu.shardingjdbcdemo.entity.OrderVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * className: OrderMapper
 * description:
 * date: 2024/6/11-19:24
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingjdbcdemo.mapper
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    /**
     * 获取订单金额
     *
     * @return {@link List<OrderVo>}
     */
    @Select({"SELECT o.order_no, SUM(i.price * i.count) AS amount FROM t_order o JOIN t_order_item i ON o.order_no = i.order_no GROUP BY o.order_no "})
    List<OrderVo> getOrderAmount();
}
