package com.atguigu.shardingjdbcdemo.mapper;

import com.atguigu.shardingjdbcdemo.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * className: UserMapper
 * description:
 * date: 2024/6/7-23:28
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingjdbcdemo.mapper
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
