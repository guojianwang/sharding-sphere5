# <center>ShardingSphere5实战教程</center>

***

## 目录

- [ShardingSphere5实战教程](#shardingsphere5实战教程)
  - [目录](#目录)
  - [笔记](#笔记)
    - [docker环境安装.md](#docker环境安装md)
    - [尚硅谷\_ShardingSphere5.md](#尚硅谷_shardingsphere5md)
  - [阿里巴巴开发手册](#阿里巴巴开发手册)
    - [Java开发手册（黄山版）.pdf](#java开发手册黄山版pdf)
  - [config](#config)
    - [mysql驱动](#mysql驱动)
    - [proxy二进制安装包](#proxy二进制安装包)
    - [proxy配置文件](#proxy配置文件)
  - [Vagrantfile](#vagrantfile)
    - [Vagrantfile](#vagrantfile-1)

***

## 笔记

***

### [docker环境安装.md](./doc/笔记/docker环境安装.md)

### [尚硅谷_ShardingSphere5.md](./doc/笔记/尚硅谷_ShardingSphere5.md)

***

## 阿里巴巴开发手册

***

### [Java开发手册（黄山版）.pdf](./doc/阿里巴巴开发手册/Java开发手册（黄山版）.pdf)

***

## config

***

### [mysql驱动](./config/mysql驱动/mysql-connector-java-8.0.22.jar)

### [proxy二进制安装包](./config/proxy二进制安装包/apache-shardingsphere-5.1.1-shardingsphere-proxy-bin.tar.gz)

### [proxy配置文件](./config/proxy配置文件/)

***

## Vagrantfile

***

### [Vagrantfile](./vagrant/Vagrantfile)

***
