package com.atguigu.shardingproxydemo;

import com.atguigu.shardingproxydemo.entity.User;
import com.atguigu.shardingproxydemo.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * className: ShardingProxyDemoApplicationTests
 * description:
 * date: 2024/6/7-23:25
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingproxydemo
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@SpringBootTest
class ShardingProxyDemoApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private UserMapper userMapper;

    /**
     * 读数据测试
     */
    @Test
    public void testSelectAll() {
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }

}
