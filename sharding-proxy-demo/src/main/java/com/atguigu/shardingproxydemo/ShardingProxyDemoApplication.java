package com.atguigu.shardingproxydemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * className: ShardingProxyDemoApplication
 * description:
 * date: 2024/6/7-23:25
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingproxydemo
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@SpringBootApplication
public class ShardingProxyDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShardingProxyDemoApplication.class, args);
    }

}
