package com.atguigu.shardingproxydemo.mapper;

import com.atguigu.shardingproxydemo.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * className: UserMapper
 * description:
 * date: 2024/6/11-23:26
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingproxydemo.mapper
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
