package com.atguigu.shardingproxydemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * className: User
 * description:
 * date: 2024/6/11-23:26
 * <p>
 * project: ShardingSphere5
 * package: com.atguigu.shardingproxydemo.entity
 * email: 1085844536@qq.com
 * version:
 *
 * @author WangGuojian
 */
@TableName("t_user")
@Data
public class User {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String uname;
}

